import styles from './alert.module.scss'
import cn from 'classnames'


export default function Alert({ children, type }) {
    return (
        <span className={cn({
            [styles.success]: type === 'success',
            [styles.error]: type === 'error', 
            [styles.alert] : true
        })} >
            {children}
        </span>
    )
}